from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/api_b', methods=['GET'])
def api_b():
    return jsonify({"message": "Hello from API B"})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
